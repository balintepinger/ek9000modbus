# This is a startup script for Beckhoff EK9000 Modbus TCP/UDP Bus Coupler monitoring EL3202 card

require ek9000modbus
require modbus

#Setting epics environment. IPPADDR can be the IP address or the hostname either. Works with both.
epicsEnvSet("IPADDR",  "ics-modbusterminal.cslab.esss.lu.se")  #Hostname of the EK9000 device.
#epicsEnvSet("IPADDR", "172.30.38.30")  #IP Address of the EK9000 device.
epicsEnvSet("IPPORT",  "502")  #Define the opened modbus port. Default setting is normally port 502.
epicsEnvSet("P",       "ai")
epicsEnvSet("R",       "1")
epicsEnvSet("PORT",    "ReadT")
epicsEnvSet("SCAN",    "1 second")

iocshLoad("$(ek9000modbus_DIR)/ek9000modbus.iocsh","IPADDR=$(IPADDR), IPPORT=$(IPPORT), $(P), R=$(R), PORT=$(PORT), SCAN=$(SCAN)" )

iocInit()
